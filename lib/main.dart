import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:navigator_3/d3.dart';
import 'package:navigator_3/position_listener.dart';
import 'package:navigator_3/second_page.dart';

import 'first_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Root(),
    );
  }
}

class Root extends HookWidget {
  const Root({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final positionNotifier = useValueNotifier(Offset.zero);
    final viewNotifier = useValueNotifier(Offset(0, -157));
    final pageSize = MediaQuery.of(context).size;
    final listenable =
        useMemoized(() => Listenable.merge([positionNotifier, viewNotifier]));
    final blockSize = useMemoized(() => pageSize.width);

    return Scaffold(
      body: PositionListener(
        onTranslate: (double x, double y) =>
            positionNotifier.value = positionNotifier.value.translate(x, y),
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onPanUpdate: (details) =>
              viewNotifier.value = viewNotifier.value + details.delta,
          child: AnimatedBuilder(
            animation: listenable,
            builder: (context, child) {
              final viewOffset = viewNotifier.value;
              final positionOffset = positionNotifier.value;
              return Transform(
                  // Transform widget
                  transform: Matrix4.identity()
                    ..setEntry(3, 2, 0.001)
                    ..rotateX(0.01 * viewOffset.dy) // changed
                    ..rotateZ(-0.01 * viewOffset.dx)
                    ..translate(positionOffset.dx, positionOffset.dy,
                        blockSize), // changed
                  alignment: FractionalOffset.bottomCenter,
                  child: child);
            },
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Positioned(
                    left: -10 * blockSize,
                    top: -10 * blockSize,
                    right: -10 * blockSize,
                    bottom: -10 * blockSize,
                    child: Image.asset(
                      'assets/dirt.png',
                      repeat: ImageRepeat.repeat,
                    )),
                Positioned(
                  top: -6 * blockSize,
                  right: -6 * blockSize,
                  child: D3(
                    child: Tree(
                      blockSize: blockSize,
                    ),
                  ),
                ),
                Positioned(
                  top: -8 * blockSize,
                  left: -3 * blockSize,
                  child: D3(
                    child: Tree(
                      blockSize: blockSize,
                    ),
                  ),
                ),
                Positioned(
                  top: -3 * blockSize,
                  right: -8 * blockSize,
                  child: D3(
                    rotation: pi / 2,
                    child: Tree(
                      blockSize: blockSize,
                    ),
                  ),
                ),
                Positioned(
                  top: -4 * blockSize,
                  child: D3(
                    child: Tree(
                      blockSize: blockSize,
                    ),
                  ),
                ),
                Positioned(
                  child: D3(
                    bottomPadding: blockSize,
                    child: SizedBox.fromSize(
                      size: pageSize,
                      child: const FirstPage(),
                    ),
                  ),
                ),
                Positioned(
                  bottom: -3 * blockSize,
                  right: -5 * blockSize,
                  child: D3(
                    rotation: pi / 2,
                    bottomPadding: blockSize,
                    child: SizedBox.fromSize(
                      size: pageSize,
                      child: const SecondPage(),
                    ),
                  ),
                ),
                Positioned(
                  bottom: -3 * blockSize,
                  right: -7 * blockSize,
                  child: D3(
                    isFlat: true,
                    bottomPadding: 4 * blockSize,
                    child: SizedBox(
                      height: blockSize * 3,
                      width: blockSize * 3,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "everything is a widget",
                              style: TextStyle(fontSize: 60),
                            ),
                            FlutterLogo(
                              size: 300,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Tree extends StatelessWidget {
  final double blockSize;

  const Tree({super.key, required this.blockSize});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: blockSize * 4,
        width: blockSize * 4,
        child: Image.asset('assets/tree.png'));
  }
}
