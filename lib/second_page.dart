import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 60,
            color: Colors.blueAccent,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: [
                    Text(
                      "Что это было?!",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ),
          CheckboxListTile(
            title: Text("Unreal Engine 6"),
            value: false,
            onChanged: (bool? value) {},
          ),
          CheckboxListTile(
            title: Text("Windows Movie Maker"),
            value: false,
            onChanged: (bool? value) {},
          ),
          CheckboxListTile(
            title: Text("Магия"),
            value: false,
            onChanged: (bool? value) {},
          ),
          Center(
            child: Text("Что бы узнать ответ посмотрите вверх"),
          )
        ],
      ),
    );
  }
}
