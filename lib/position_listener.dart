import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:navigator_3/persistent_frame_callback.dart';

class PositionListener extends StatefulWidget {
  final Widget child;
  final Function(double, double) onTranslate;

  const PositionListener({
    Key? key,
    required this.child,
    required this.onTranslate,
  }) : super(key: key);

  @override
  State<PositionListener> createState() => _PositionListenerState();
}

class _PositionListenerState extends State<PositionListener> {
  final _pressedKeys = <int>{};

  @override
  void initState() {
    super.initState();
    PersistentFrameCallback.addPersistentFrameCallback(_onFrame);
    window.onKeyData = _onKey;
  }

  @override
  void dispose() {
    PersistentFrameCallback.removePersistentFrameCallback(_onFrame);
    window.onKeyData = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  void _onFrame(Duration duration) {
    if (_pressedKeys.contains(_w)) {
      widget.onTranslate(0, duration.inMilliseconds * speed);
    } else if (_pressedKeys.contains(_s)) {
      widget.onTranslate(0, -duration.inMilliseconds * speed);
    }
    if (_pressedKeys.contains(_d)) {
      widget.onTranslate(-duration.inMilliseconds * speed, 0);
    } else if (_pressedKeys.contains(_a)) {
      widget.onTranslate(duration.inMilliseconds * speed, 0);
    }
  }

  bool _onKey(KeyData data) {
    if (data.type == KeyEventType.down) {
      _pressedKeys.add(data.logical);
      setState(() {});
    } else if (data.type == KeyEventType.up) {
      _pressedKeys.remove(data.logical);
      setState(() {});
    }
    return false;
  }

  static const _w = 119;
  static const _a = 97;
  static const _s = 115;
  static const _d = 100;
  static const speed = 1.5;
}
