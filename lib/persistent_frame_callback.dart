class PersistentFrameCallback {
  final _callbacks = <void Function(Duration timeStamp)>{};
  static final instance = PersistentFrameCallback._();

  PersistentFrameCallback._() {
    const duration = Duration(milliseconds: 16);
    Stream.periodic(duration).listen((event) {
      _onFrame(duration);
    });
  }

  _onFrame(Duration timeStamp) {
    for (final callback in _callbacks) {
      callback(timeStamp);
    }
  }

  static addPersistentFrameCallback(
          void Function(Duration timeStamp) callback) =>
      instance._callbacks.add(callback);

  static removePersistentFrameCallback(
          void Function(Duration timeStamp) callback) =>
      instance._callbacks.remove(callback);
}
