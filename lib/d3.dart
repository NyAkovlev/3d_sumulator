import 'dart:math';

import 'package:flutter/cupertino.dart';

class D3 extends StatelessWidget {
  final Widget child;
  final double bottomPadding;
  final double rotation;
  final bool isFlat;

  const D3({
    super.key,
    required this.child,
    this.bottomPadding = 0,
    this.rotation = 0,
    this.isFlat = false,
  });

  @override
  Widget build(BuildContext context) {
    final matrix = Matrix4.identity();
    if (!isFlat) {
      matrix
        ..rotateX(pi / 2)
        ..rotateY(rotation);
    } else {
      matrix.rotateY(pi);
      matrix.rotateZ(pi / 2);
    }

    matrix.translate(
      0.0,
      isFlat ? 0 : -bottomPadding,
      isFlat ? bottomPadding : 0,
    );
    return Transform(
      transform: matrix,
      alignment: FractionalOffset.bottomCenter,
      child: child,
    );
  }
}
